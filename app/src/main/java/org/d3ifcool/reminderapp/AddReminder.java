package org.d3ifcool.reminderapp;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.DatePickerDialog;
import android.app.PendingIntent;
import android.app.TimePickerDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TimePicker;
import android.widget.Toast;

import org.d3ifcool.reminderapp.Data.DbContract;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Calendar;


public class AddReminder extends AppCompatActivity {
    EditText editTextJudul, editTextDesk;
    ImageView mReminder;
    private String mDate, mTime, mDateReminder, mTimeReminder;
    private byte[] mImageUpload;
    private Boolean isImageSet = false;
    public static final int PICK_IMAGE = 1;
    private Calendar mCalendarReminder = Calendar.getInstance();
    private Boolean isReminderSet = false;

    DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear,
                              int dayOfMonth) {
            mCalendarReminder.set(Calendar.YEAR, year);
            mCalendarReminder.set(Calendar.MONTH, monthOfYear);
            mCalendarReminder.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            timePick();
        }
    };

    TimePickerDialog.OnTimeSetListener time = new TimePickerDialog.OnTimeSetListener() {
        @Override
        public void onTimeSet(TimePicker timePicker, int hourOfDay, int minute) {
            mCalendarReminder.set(Calendar.HOUR, hourOfDay);
            mCalendarReminder.set(Calendar.MINUTE, minute);
            toast();
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_reminder);

        editTextJudul = (EditText) findViewById(R.id.ediTextJudul);
        editTextDesk = (EditText) findViewById(R.id.editTextDesk);
        mReminder = (ImageView) findViewById(R.id.reminder_imageView);

        mReminder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new DatePickerDialog(AddReminder.this, date, mCalendarReminder
                        .get(Calendar.YEAR), mCalendarReminder.get(Calendar.MONTH),
                        mCalendarReminder.get(Calendar.DAY_OF_MONTH)).show();
            }
        });
    }

    public void timePick(){
        new TimePickerDialog(AddReminder.this, time, mCalendarReminder.get(Calendar.HOUR), mCalendarReminder.get(Calendar.MINUTE),false).show();
    }

    public void toast(){
        mDateReminder = new SimpleDateFormat("dd MMMM yyyy").format(mCalendarReminder.getTime());
        mTimeReminder =  new SimpleDateFormat("HH:mm").format(mCalendarReminder.getTime());
        String dateReminder = String.valueOf(mDateReminder);
        String timeReminder = String.valueOf(mTimeReminder);
        isReminderSet = true;
        Toast.makeText(this, "Pengingat : " + dateReminder + ", Pada Jam : " + timeReminder, Toast.LENGTH_SHORT).show();
    }

    public void btClip(View view) {
        Intent intent = new Intent(Intent.ACTION_PICK);
        intent.setType("image/*");
        startActivityForResult(intent, PICK_IMAGE);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PICK_IMAGE && resultCode == Activity.RESULT_OK && data != null){
            Toast.makeText(this,"Berhasil Upload Gambar",Toast.LENGTH_SHORT).show();
            isImageSet = true;
            Uri uri = data.getData();
            try {
                InputStream inputStream = getContentResolver().openInputStream(uri);
                Bitmap bitmap = BitmapFactory.decodeStream(inputStream);
                bitmap = getResizedBitmap(bitmap,400);
                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
                mImageUpload = stream.toByteArray();
            }catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_add, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.done){
            simpanRimender();
            finish();

        }
        return super.onOptionsItemSelected(item);
    }

    public Bitmap getResizedBitmap(Bitmap image, int maxSize) {
        int width = image.getWidth();
        int height = image.getHeight();

        float bitmapRatio = (float)width / (float) height;
        if (bitmapRatio > 1) {
            width = maxSize;
            height = (int) (width / bitmapRatio);
        } else {
            height = maxSize;
            width = (int) (height * bitmapRatio);
        }
        return Bitmap.createScaledBitmap(image, width, height, true);
    }

    private void simpanRimender(){
        if (editTextDesk.getText().toString().equals("")) {
            Toast.makeText(this, "Semua Data Harus di isi", Toast.LENGTH_SHORT).show();
            return;
        }

        if (!isImageSet){
            Resources res = getResources();
            Drawable drawable = res.getDrawable(R.drawable.ic_launcher_background);
            Bitmap bitmap = ((BitmapDrawable)drawable).getBitmap();
            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, stream);
            mImageUpload = stream.toByteArray();
        }

        ContentValues values = new ContentValues();
        values.put(DbContract.ReminderEntry.COLUMN_NAMA, editTextJudul.getText().toString());
        values.put(DbContract.ReminderEntry.COLUMN_DESK, editTextDesk.getText().toString());
        values.put(DbContract.ReminderEntry.COLUMN_IMAGE, mImageUpload);
        values.put(DbContract.ReminderEntry.COLUMN_DATE, mDateReminder);
        values.put(DbContract.ReminderEntry.COLUMN_TIME, mTimeReminder);
        values.put(DbContract.ReminderEntry.COLUMN_STATUS, 1);

        Log.d("AddReminder", "simpanRimender: " + mImageUpload);

        Uri uri = getContentResolver().insert(DbContract.ReminderEntry.CONTENT_URI, values);
        if (uri == null) {
            Toast.makeText(this, "Gagal menyimpan", Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(this, "Berhasil Simpan Reminder ", Toast.LENGTH_SHORT).show();
        }

        //Set Reminder
        String judul = editTextJudul.getText().toString();
        if (isReminderSet) {
            AlarmManager alarmManager = (AlarmManager) this.getSystemService(Context.ALARM_SERVICE);
            Intent intentAlarmReceiver = new Intent(this, AlarmReceiver.class);
            intentAlarmReceiver.putExtra("title", judul);
            intentAlarmReceiver.putExtra("reminder", "" + mDateReminder + " " + mTimeReminder);
            final int _id = (int) System.currentTimeMillis();
            PendingIntent pendingIntent = PendingIntent.getBroadcast(this, _id, intentAlarmReceiver, 0);
            alarmManager.set(AlarmManager.RTC, mCalendarReminder.getTimeInMillis(), pendingIntent);
        }
    }
}