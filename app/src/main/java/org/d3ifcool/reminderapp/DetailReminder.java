package org.d3ifcool.reminderapp;

import android.content.DialogInterface;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import org.d3ifcool.reminderapp.Data.DbContract;

public class DetailReminder extends AppCompatActivity implements LoaderManager.LoaderCallbacks<Cursor> {
    private static final int EXISTING_LIST_LOADER = 0;
    private Uri mCurrentUri;
    TextView judul,isi,date,time;
    ImageView image;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_reminder);

        judul = (TextView) findViewById(R.id.judulDetail);
        isi = (TextView) findViewById(R.id.deskripsiDetail);
        date = (TextView) findViewById(R.id.tanggalDetail);
        time = (TextView) findViewById(R.id.waktuDetail);
        image = (ImageView) findViewById(R.id.imageDetail);


        mCurrentUri = getIntent().getData();
        if (mCurrentUri != null) {
            getSupportLoaderManager().initLoader(EXISTING_LIST_LOADER,null ,this);
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_detail, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.delete){
            deleteReminder();
        }
        return super.onOptionsItemSelected(item);
    }

    private void deleteReminder(){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Hapus Reminder Ini ?");
        builder.setPositiveButton("Hapus", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                getContentResolver().delete(mCurrentUri, null,null);

                Toast.makeText(DetailReminder.this,"Berhasil Hapus",Toast.LENGTH_SHORT).show();
                finish();
            }
        });
        builder.setNegativeButton("Batal", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });
        builder.create().show();
    }


    @NonNull
    @Override
    public Loader<Cursor> onCreateLoader(int id, @Nullable Bundle args) {
        String[] projection = {
                DbContract.ReminderEntry._ID,
                DbContract.ReminderEntry.COLUMN_NAMA,
                DbContract.ReminderEntry.COLUMN_DESK,
                DbContract.ReminderEntry.COLUMN_IMAGE,
                DbContract.ReminderEntry.COLUMN_DATE,
                DbContract.ReminderEntry.COLUMN_TIME,
                DbContract.ReminderEntry.COLUMN_STATUS,
        };

        return new CursorLoader(this, mCurrentUri, projection,
                null, null, null);
    }

    @Override
    public void onLoadFinished(@NonNull Loader<Cursor> loader, Cursor data) {
        if (data.moveToFirst()){
            int judulColumnIndex = data.getColumnIndex(DbContract.ReminderEntry.COLUMN_NAMA);
            int isiCOlumnINdex = data.getColumnIndex(DbContract.ReminderEntry.COLUMN_DESK);
            int dateColumnIndex = data.getColumnIndex(DbContract.ReminderEntry.COLUMN_DATE);
            int timeColumnIndex = data.getColumnIndex(DbContract.ReminderEntry.COLUMN_TIME);
            int img = data.getColumnIndex(DbContract.ReminderEntry.COLUMN_IMAGE);

            judul.setText(data.getString(judulColumnIndex));
            isi.setText(data.getString(isiCOlumnINdex));
            date.setText(data.getString(dateColumnIndex));
            time.setText(data.getString(timeColumnIndex));

            byte[] byteArray = data.getBlob(img);

            Bitmap bm = BitmapFactory.decodeByteArray(byteArray, 0 ,byteArray.length);
            image.setImageBitmap(bm);

        }
    }

    @Override
    public void onLoaderReset(@NonNull Loader<Cursor> loader) {

    }
}
