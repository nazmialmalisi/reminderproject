package org.d3ifcool.reminderapp;

import android.content.ContentUris;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;

import org.d3ifcool.reminderapp.R;

import org.d3ifcool.reminderapp.Data.DbContract;

public class MainActivity extends AppCompatActivity implements LoaderManager.LoaderCallbacks<Cursor> {
    ListView mListView;
    private CursorAdapter mAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mAdapter = new CursorAdapter(this,null);

        mListView = (ListView) findViewById(R.id.listview);
        mListView.setAdapter(mAdapter);

        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Intent intent = new Intent(MainActivity.this, DetailReminder.class);
                Uri currentListUri = ContentUris.withAppendedId(DbContract.ReminderEntry.CONTENT_URI, l);
                intent.setData(currentListUri);
                startActivity(intent);
            }
        });

        getSupportLoaderManager().initLoader(0,null,this);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_menu, menu);
        return true;

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.tambah){
            Intent intent = new Intent(MainActivity.this, AddReminder.class);
            startActivity(intent);
        }
        return super.onOptionsItemSelected(item);
    }

    @NonNull
    @Override
    public Loader<Cursor> onCreateLoader(int i, @Nullable Bundle bundle) {
        String[] projection = {
                DbContract.ReminderEntry._ID,
                DbContract.ReminderEntry.COLUMN_NAMA,
                DbContract.ReminderEntry.COLUMN_DESK,
                DbContract.ReminderEntry.COLUMN_IMAGE,
                DbContract.ReminderEntry.COLUMN_DATE,
                DbContract.ReminderEntry.COLUMN_TIME,
                DbContract.ReminderEntry.COLUMN_STATUS,
        };
//        String selection = DbContract.ReminderEntry.COLUMN_TANGGAL_SHOLAT + "=?";
//        String selectionArg[] = new String[]{"" + DateTimeUtils.dateOfTheDayInStringTampil()};
        return new CursorLoader(this, DbContract.ReminderEntry.CONTENT_URI, projection,
                null, null, null);
    }

    @Override
    public void onLoadFinished(@NonNull Loader<Cursor> loader, Cursor cursor) {
        if (cursor.moveToFirst()){
            mAdapter.swapCursor(cursor);

        } else {

        }
    }

    @Override
    public void onLoaderReset(@NonNull Loader<Cursor> loader) {
        mAdapter.swapCursor(null);
    }
}
