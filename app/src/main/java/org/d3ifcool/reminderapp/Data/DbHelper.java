package org.d3ifcool.reminderapp.Data;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DbHelper extends SQLiteOpenHelper {

    private static final String DATABASE_NAME = "reminder.db";
    private static final int DATABASE_VERSION = 1;

    public DbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String SQL_CREATE_TABLE_REMINDER =  "CREATE TABLE " + DbContract.ReminderEntry.TABLE_NAME + " ("
                + DbContract.ReminderEntry._ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                + DbContract.ReminderEntry.COLUMN_NAMA + " TEXT NOT NULL, "
                + DbContract.ReminderEntry.COLUMN_DESK + " TEXT NOT NULL, "
                + DbContract.ReminderEntry.COLUMN_IMAGE + " BLOB, "
                + DbContract.ReminderEntry.COLUMN_DATE + " TEXT NOT NULL, "
                + DbContract.ReminderEntry.COLUMN_TIME + " TEXT NOT NULL, "
                + DbContract.ReminderEntry.COLUMN_STATUS+ " INTEGER);";

        db.execSQL(SQL_CREATE_TABLE_REMINDER);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}
