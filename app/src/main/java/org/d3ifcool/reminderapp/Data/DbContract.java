package org.d3ifcool.reminderapp.Data;

import android.net.Uri;
import android.provider.BaseColumns;

public class DbContract  {

    private DbContract(){}

    public static final String CONTENT_AUTHORITY = "org.d3ifcool.reminderapp";

    public static final Uri BASE_CONTENT_URI = Uri.parse("content://" + CONTENT_AUTHORITY);

    public static final String PATH_REMINDER = "reminder";

    public static final class ReminderEntry implements BaseColumns {
        public static final Uri CONTENT_URI = Uri.withAppendedPath(BASE_CONTENT_URI, PATH_REMINDER);

        public final static String TABLE_NAME = "reminder";

        public final static String _ID = BaseColumns._ID;

        public final static String COLUMN_NAMA = "namaReminder";
        public final static String COLUMN_DESK = "deskReminder";
        public final static String COLUMN_IMAGE = "imageReminder";
        public final static String COLUMN_DATE = "dateReminder";
        public final static String COLUMN_TIME = "timeReminder";
        public final static String COLUMN_STATUS = "statusReminder";
    }

}
