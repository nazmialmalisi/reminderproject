package org.d3ifcool.reminderapp;

import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import org.d3ifcool.reminderapp.Data.DbContract;

public class CursorAdapter extends android.support.v4.widget.CursorAdapter {

    public CursorAdapter(Context context, Cursor c) {
        super(context, c, 0);
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        return LayoutInflater.from(context).inflate(R.layout.list_item, parent, false);
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        TextView judul = (TextView) view.findViewById(R.id.judul);
        judul.setText(cursor.getString(cursor.getColumnIndex(DbContract.ReminderEntry.COLUMN_NAMA)));

        TextView desk = (TextView) view.findViewById(R.id.desk);
        desk.setText(cursor.getString(cursor.getColumnIndex(DbContract.ReminderEntry.COLUMN_DESK)));

        byte[] byteArray = cursor.getBlob(cursor.getColumnIndex(DbContract.ReminderEntry.COLUMN_IMAGE));

        Bitmap bm = BitmapFactory.decodeByteArray(byteArray, 0 ,byteArray.length);
        ImageView project_imageView = (ImageView) view.findViewById(R.id.img);
        //project_imageView.setImageResource(R.mipmap.senarai_launcher_icon);
        project_imageView.setImageBitmap(bm);
    }
}
